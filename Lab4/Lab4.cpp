// Lab4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <iostream>
#include "Segmentation.h"

using namespace cv;
using namespace std;

int main()
{
	Mat img = imread("..\\DATA\\Lab4\\input.png");
	Segmentation seg(img);
}

