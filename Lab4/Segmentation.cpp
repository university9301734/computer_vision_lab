#include "stdafx.h"
#include "Segmentation.h"

Segmentation::Segmentation(Mat _img, enum selection _s, enum type_op _op) :
	img(_img),
	s(_s),
	op(_op)
{
	CV_Assert(s != OUTER_AREA | INNER_AREA);

	if (_img.type() != CV_8UC1)
	{
		try
		{
			cvtColor(_img, img, CV_BGR2GRAY);
		}
		catch (...)
		{
			cout << "[SEGMENTATION] Conversion from color to gray went wrong. Aborting." << endl;
		}
	}
}

void Segmentation::execute()
{
	resetMask();
	doCanny();
	if (op & LINES || op & CIRCLES_AND_LINES)
		doLineSegment();
	if (op & CIRCLES || op & CIRCLES_AND_LINES)
		doCircleSegment();
}


Mat Segmentation::drawLines(const Mat& img)
{
	return img.clone();
}

Mat Segmentation::drawCircles(const Mat& img)
{
	return img.clone();
}

void Segmentation::doCanny()
{
	const string w_name = WIN_NAME + " SETUP";
	namedWindow(w_name);
	int th1 = 10, th2 = 10, size = 3;
	createTrackbar("TH1", w_name, &th1, 100);
	createTrackbar("TH2", w_name, &th2, 100);
	createTrackbar("SIZE", w_name, &size, 7);

	do
	{
		size = size < 3 ? 3 : size % 2 == 0 ? size + 1 : size;
		Canny(img, canny_img, th1 / 100.0 * 255, th2 / 100.0 * 255, size, false);

		imshow(w_name, canny_img);
	} while (waitKey(100) != 27);

	destroyWindow(w_name);
}

void Segmentation::doCircleSegment()
{
	const string w_name = WIN_NAME + " CIRLCES";
	namedWindow(w_name);

	vector<Vec3f> circles;
	do
	{
		//HoughCircles(canny_img, circles, );

		Mat tmp = drawCircles(img);
		imshow(w_name, tmp);
	} while (waitKey(100) != 27);

	destroyWindow(w_name);
}

void Segmentation::doLineSegment()
{
	const string w_name = WIN_NAME + " LINES";
	namedWindow(w_name);

	int rho = 10, theta = 10, th = 10;
	createTrackbar("RHO", w_name, &rho, 100);
	createTrackbar("THETA", w_name, &theta, 100);
	createTrackbar("THRESHOLD", w_name, &th, 100);

	double max_rho = std::sqrt(img.cols*img.cols + img.rows*img.rows);
	
	vector<Vec2f> lines;
	do
	{
		HoughLines(canny_img, lines, rho / 100.0 * max_rho, theta / 100.0 * CV_PI, cvCeil(th / 100.0 * 1000));

		Mat tmp = drawLines(img);
		imshow(w_name, tmp);
	} while (waitKey(100) != 27);

	destroyWindow(w_name);
}

Mat Segmentation::getMask()
{
	return mask.clone();
}


void Segmentation::resetMask()
{
	mask.release();
	mask = Mat::zeros(img.size(), CV_8UC1);
}

Segmentation::~Segmentation()
{
	mask.release();
	img.release();
	canny_img.release();
}