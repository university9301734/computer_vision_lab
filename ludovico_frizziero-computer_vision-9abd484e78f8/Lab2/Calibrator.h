#pragma once

#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/utils/filesystem.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <Windows.h>
#include <string.h>


using namespace std;
using namespace cv;

enum Calibrator_flags
{
	USE_CIRCLE_GRID = 0x01,
	USE_LIVE_CAM_FEED = USE_CIRCLE_GRID << 1,
	USE_VIDEO_FILE = USE_LIVE_CAM_FEED << 1,
	USE_IMAGE_SEQUENCE = USE_VIDEO_FILE << 1,
	USE_FULL_OUTPUT = USE_IMAGE_SEQUENCE << 1 //return calibration error statistics and extrinsic parameters
#ifdef _DEBUG
	, OVERRIDE_MANUAL_DEBUG = USE_FULL_OUTPUT << 1 //OVERRIDE the press 'n' on imshow() in from_storage()
#endif // _DEBUG
};

struct calibration
{
	Mat cam_mat;
	Mat dist_coeffs;
	bool full_output;

	//extrinsic parameters (see cv::calibrateCamera())
	int tot_grid_views = 0;
	vector<Mat> rvecs;
	vector<Mat> tvecs;

	//calibration error statistics (see cv::calibrateCamera())
	vector<double> stdDevInt;
	vector<double> stdDevExt;
	vector<double> perViewError;
};

#ifdef _DEBUG
#define WIN_NAME "CALIBRATOR-DBG"
#else
#define WIN_NAME "CALIBRATOR"
#endif

class Calibrator
{
private:
	const int x, y, f, m;
	const float l;
	VideoCapture* cap = nullptr;
	vector<string> a;

	bool inline dirExists(const std::string& dirName_in)
	{
#ifdef _WINDOWS_
		DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
		if (ftyp == INVALID_FILE_ATTRIBUTES)
			return false;

		if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
			return true;

		return false;
#else
		return true;
#endif
	}

	vector<Point3f> createChessGrid();
	vector<Point3f> createCircleGrid();

	//For still imgs or saved video
	int from_storage(vector< vector<Point2f> >& corners, Size& img_size);

	int from_live_feed(vector< vector<Point2f> >& corners, Size& img_size);


public:
	/**
		x_cells: number of cells in the grid's x direction.
		y_cells: number of cells in the grid's y direction.
		length: caracteristic length that defines the grid (e.g. one cell edge for chessboard grids).
		args: variable list of arguments that depends on flags, all encoded as strings.
		flags: settings for the Calibrator.
	*/
	Calibrator(int width, int heigth, float length, const vector<string>& args, int flags = USE_IMAGE_SEQUENCE, int max_live_snapshots = 15);

	/**
		return mean calibration error (see cv::CalibrateCamera())
	*/
	double calibrate(struct calibration* out_res);

	static
	void save_to_file(const string path, struct calibration* c);

	/**
		WARNING: out_res MUST be preallocated.
	*/
	static
	void load_from_file(const string path, struct calibration* out_res);

	~Calibrator();
};

