// Lab2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Calibrator.h"
#include <iostream>

using namespace std;
using namespace cv;


int main(int argc, char** argv)
{
	int x_cells = 6, y_cells = 5;
	int flags = USE_IMAGE_SEQUENCE | USE_FULL_OUTPUT;
#ifdef _DEBUG 
	flags |= OVERRIDE_MANUAL_DEBUG;
#endif // _DEBUG 

#define IMG_SEQ { "..\\DATA\\Lab2", "%04d_color.png" } // "img%0d.jpg"  
#define VID_SEQ { "..\\DATA\\Lab2\\somevideo.avi" }
#define LIV_SEQ { "0" }

	Calibrator calibrator(x_cells, y_cells, 1, IMG_SEQ, flags);

	struct calibration* calib = new struct calibration;
	double avgErr = calibrator.calibrate(calib);

	cout << "Avg Error: " << to_string(avgErr) << endl;

	for (size_t i = 0; i < calib->perViewError.size() && calib->full_output; i++)
	{
		cout << "img " << to_string(i) << " error: " << to_string(calib->perViewError[i]) << endl;
	}

	Calibrator::save_to_file("..\\DATA\\Lab2\\calib.yml", calib);

#ifdef _DEBUG 
	struct calibration* test_calib = new struct calibration;
	Calibrator::load_from_file("..\\DATA\\Lab2\\calib.yml", test_calib); //just test load routine
	cout << "DEBUG: loaded calibration\n" << "\t tot views: " + to_string(test_calib->tot_grid_views) + "\n" << "\t fulloutput : " + to_string(test_calib->full_output)<< endl;
	delete(test_calib);
#endif // _DEBUG

	namedWindow("UNDISTORTED", CV_WINDOW_NORMAL);
	Mat m = imread("..\\DATA\\Lab2\\test_image.png"), to_show( Size(m.size().width * 2, m.size().height), CV_8UC3);
	m.copyTo(to_show(Rect(0, 0, m.size().width, m.size().height)));
	
	//undistort(m.clone(), m, calib->cam_mat, calib->dist_coeffs);	
	Mat newMatCam = calib->cam_mat.clone(), map1, map2;
	initUndistortRectifyMap(calib->cam_mat, calib->dist_coeffs, noArray(), newMatCam, m.size(), CV_32FC1, map1, map2);
	remap(m, m, map1, map2, INTER_LINEAR);
	
	m.copyTo(to_show(Rect(m.size().width, 0, m.size().width, m.size().height)));
	imshow("UNDISTORTED", to_show);
	//imwrite("..\\DATA\\Lab2\\result.png", to_show);
	waitKey(0);

	/*cout << "PRESS any <key + enter> to exit" << endl;
	string s;
	cin >> s;*/

	//-------------------------------------------------------free memory----------------------------------------------------------
	delete calib; 
	return 0;
}

