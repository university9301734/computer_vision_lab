// Lab3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat drawHist(vector<Mat> hists)
{

	if (hists.size() > 3)
	{
		cout << "[DRAWHIST] error" << endl;
		return Mat();
	}

	int hist_w = 512; 
	int hist_h = hists[0].rows; //do to normalization
	int histSize = hists[0].rows;
	int bin_w = cvRound((double)hist_w / histSize);

	Mat img = Mat::zeros(hist_h, bin_w * histSize, CV_8UC3);
	int channel = 0;
	for (int c = 0; c < hists.size(); c++)
	{
		Mat& h = hists[c];
		for (int i = 1; i < histSize; i++)
		{
			cout << cvRound(h.at<float>(i - 1)) << endl;
			line(img, 
				Point(bin_w*(i - 1), hist_h - cvRound(h.at<float>(i - 1))),
				Point(bin_w*(i), hist_h - cvRound(h.at<float>(i))),
				Scalar((channel == 0) * 255, (channel == 1) * 255, (channel == 2) * 255), 
				2, 8, 0);
		}
		channel++;
	}
	return img;
}

vector<Mat> execute(Mat src, bool equalize_image = true, vector<bool> channel_mask = {1, 1, 1})
{
	Mat b_hist, g_hist, r_hist;
	vector<Mat> bgr_src(3);
	split(src, bgr_src);
	int bins = 256;
	float range[] = { 0, 255 };
	const float* histRange = { range };

	if (equalize_image)
	{
		for (int i = 0; i < bgr_src.size(); i++)
		{
			if (channel_mask[i])
				equalizeHist(bgr_src[i].clone(), bgr_src[i]);
		}

		merge(bgr_src, src);		
	}

	if (channel_mask[0]) calcHist(&bgr_src[0], 1, 0, Mat(), b_hist, 1, &bins, &histRange, true, false);
	if (channel_mask[1]) calcHist(&bgr_src[1], 1, 0, Mat(), g_hist, 1, &bins, &histRange, true, false);
	if (channel_mask[2]) calcHist(&bgr_src[2], 1, 0, Mat(), r_hist, 1, &bins, &histRange, true, false);

	//Normalize histograms for display
	if (channel_mask[0]) normalize(b_hist, b_hist, 0, bins, NORM_MINMAX);
	if (channel_mask[1]) normalize(g_hist, g_hist, 0, bins, NORM_MINMAX);
	if (channel_mask[2]) normalize(r_hist, r_hist, 0, bins, NORM_MINMAX);

	return { src, 
		channel_mask[0] ? b_hist : Mat(), 
		channel_mask[1] ? g_hist : Mat(),
		channel_mask[2] ? r_hist : Mat() 
	};
}

void stat_filter(InputArray src, OutputArray dst, InputArray kernel, Point anchor = Point(-1, -1), int iterations = 1, int borderType = BORDER_CONSTANT, const Scalar &borderValue = morphologyDefaultBorderValue())
{

}

int main()
{
	//HW point 1
	Mat src = imread("..\\DATA\\Lab3\\lab3_image.jpg", CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
	namedWindow("OUT-img");
	namedWindow("OUT-hists");

	// HW point 2
	vector<Mat> res = execute(src.clone(), false);
	imshow("OUT-img", res[0]);
	imshow("OUT-hists", drawHist({ res[1], res[2], res[3] }));
	waitKey(0);

	// HW point 3-4
	res = execute(src.clone(), true);
	imshow("OUT-img", res[0]);
	imshow("OUT-hists", drawHist({ res[1], res[2], res[3] }));
	waitKey(0);

	// HW point 5 
	Mat Lab_img, tmp;
	cvtColor(src, Lab_img, CV_BGR2Lab);
	res = execute(Lab_img, true, { 1, 0, 0 });
	cvtColor(res[0], tmp, CV_Lab2BGR);
	imshow("OUT-img", tmp);
	imshow("OUT-hists", drawHist({ res[1] }));
	waitKey(0);

	// HW point 5 bis
	Mat hsv_img;
	cvtColor(src, hsv_img, CV_BGR2HSV);
	res = execute(hsv_img, true, { 0, 0, 1 });
	cvtColor(res[0], tmp, CV_HSV2BGR);
	imshow("OUT-img", tmp);
	imshow("OUT-hists", drawHist({ res[3] }));
	waitKey(0);


	// HW point 6
	destroyWindow("OUT-hists");
	destroyWindow("OUT-img");
	namedWindow("OUT-img", CV_WINDOW_NORMAL);

	int f_size = 1, mode = 0;
	createTrackbar("Filter Size", "OUT-img", &f_size, 7);
	createTrackbar("Mode", "OUT-img", &mode, 7);

	do 
	{
		f_size = f_size < 1 ? 1 : f_size; 
		mode = mode > 7 ? 7 : (mode < 0 ? 0 : mode); // clamp mode between 0 and 7 (see opencv image proc -> morphTypes)
		int x = 2 * f_size + 1;

		Mat to_disp, kernel = getStructuringElement(MORPH_RECT, Size(x, x));
		morphologyEx(tmp.clone(), to_disp, mode, kernel);

		imshow("OUT-img", to_disp);

	} while (waitKey(100) < 0);

}
