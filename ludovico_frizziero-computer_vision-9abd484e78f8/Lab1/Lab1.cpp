// Lab1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/cuda.hpp>
#include <mutex>
#include <thread>
#include "Worker.h"

#define WIN_NAME "OUTPUT"

using namespace cv;
using namespace std;

mutex mtx;
condition_variable cond;

void callback(int event, int x, int y, int flags, void* data)
{
	unique_lock<mutex> lck(mtx);

	if (event == EVENT_LBUTTONDOWN)
	{
#ifdef _DEBUG
		cout << "clicked lmouse: " << x << " " << y << endl;
#endif // _DEBUG
			
		//keep callback as fast as possible
		Worker* w = (Worker*)data;
		w->setMouseInfo(event, x, y);
		cond.notify_one();
	}
	else if (event == EVENT_RBUTTONDOWN)
	{
#ifdef _DEBUG
		cout << "clicked rmouse" << endl;
#endif // _DEBUG		

		Worker* w = (Worker*)data;
		w->setMouseInfo(event, x, y);
		cond.notify_one();
	}
}

int main(int argc, char** argv)
{
	string img_name = "..\\DATA\\Lab1\\robocup.jpg"; 
	if (argc == 2)
		img_name = std::string(argv[1]);

	Mat img_h, src = imread(img_name);
	img_h = src.clone();
	if (src.empty())
	{
		cerr << "error: invalid image path" << endl;
		return 1;
	}

	Worker* w = new Worker(&img_h, &mtx, &cond);

	namedWindow(WIN_NAME);
	setMouseCallback(WIN_NAME, callback, (void*)w);

	char b = 0;
	while (b != 27) //27 == ESC key
	{
#ifdef _DEBUG
		cout << "main cicle" << endl;
#endif
		
		imshow(WIN_NAME, img_h);
		b = waitKey(100);
	}

	//-----------------------free memory----------------------------
	w->stop_work();
	delete(w);

	return 0;
}

