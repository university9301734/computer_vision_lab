#pragma once

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudaarithm.hpp>
#include <mutex>
#include <thread>
#include <iostream>

using namespace cv;
using namespace std;

class Worker
{
private:
	mutex* mtx;
	condition_variable* cond;
	Mat* img_h;
	thread t;
	bool done;
	int event, x, y;

	void do_work();

public:
	Worker(Mat* src, mutex* m, condition_variable* c);
	~Worker();

	void setMouseInfo(int event, int x, int y);
	void stop_work();
};

