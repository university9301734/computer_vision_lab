#pragma once
#pragma once

#include <opencv2/highgui/highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

enum selection {OUTER_AREA=0x01, INNER_AREA=OUTER_AREA+0x01};
enum type_op {LINES = 0x01, CIRCLES=LINES+0x01, CIRCLES_AND_LINES = LINES | CIRCLES};

class Segmentation
{
private:
	const string WIN_NAME = "SEGMENT";
	Mat img, canny_img, mask;
	enum selection s;
	enum type_op op;

	void doCanny();
    
	void doLineSegment();

	void doCircleSegment();
	
	Mat drawLines(const Mat& img);
	
	Mat drawCircles(const Mat& img);

public:
	Segmentation(Mat _img, enum selection _s, enum type_op _op);
	
	void execute();

	Mat getMask();

	void resetMask();

	~Segmentation();
};

