#include "stdafx.h"
#include "PanoramicImage.h"

using namespace std;
using namespace cv;

PanoramicImage::PanoramicImage(const string pattern, const int start, const double angle)
{
	load_images(pattern, start, angle);
}


Mat PanoramicImage::createPanorama()
{
	findSURF();

	float min_t_y = 1000, max_t_y = 0;
	for each (const auto t in trasl)
	{
		if (t.y < min_t_y)
			min_t_y = t.y;
		if (t.y > max_t_y)
			max_t_y = t.y;
	}

	int excess = 20;
	int width = imgs_d[0].cols, heigth = imgs_d[0].rows;
	cuda::GpuMat pano(Size(width * imgs_d.size(), heigth + 2 * excess), imgs_d[0].type(), Scalar(0, 0, 0));
	imgs_d[0].copyTo(pano(Rect( Point(0, excess), imgs_d[0].size() )));
	for (int i = 1; i < imgs_d.size(); i++)
	{	
		auto orig = Point2f(trasl[i - 1].x, trasl[i - 1].y + excess);
		float debug = orig.y + heigth;
		imgs_d[i].copyTo(pano(Rect(orig, imgs_d[i].size())));
	}

	Mat p;
	pano.download(p);
	pano.release();

	//remove seams (... almost ...)
	int k_size = 5;
	int bound = 10;
	for each (const auto t in trasl)
	{
		auto roi = Rect(Point2f(t.x -  bound/ 2, 0), Size(bound, p.rows));
		medianBlur(p(roi), p(roi), k_size);
	}

	return p(Rect(0, excess + 5, trasl.back().x + width, heigth - 5)); //remove unwanted black pixels at image borders
}

//Mat PanoramicImage::createPanorama2()
//{
//	vector<Mat> imgs;
//	for each (auto img in imgs_d)
//	{
//		Mat i;
//		img.download(i);
//		imgs.push_back(i);
//	}
//	Mat pano;
//	Ptr<Stitcher> stitcher = Stitcher::create(Stitcher::PANORAMA, false);
//	Stitcher::Status status = stitcher->stitch(imgs, pano);
//	if (pano.empty())
//		return Mat::zeros(100, 100, CV_8UC3); //TODO: why pano is empty???
//	else
//		return pano;
//}


void PanoramicImage::findSURF()
{
	double hessian = 1500;
	int octave = 4, octLayers = 4;
	cuda::SURF_CUDA surf(hessian, octave, octLayers, true); //use extended 128 bit descriptors
	Ptr<cuda::DescriptorMatcher> matcher = cuda::DescriptorMatcher::createBFMatcher(NORM_L2);
	Point2f offset_accumulator(0, 0);

	cuda::GpuMat kp1_d, dscr1_d;
	cuda::GpuMat& img1_d = imgs_d[0];
	//surf(img1_d.clone(), cuda::GpuMat(), kp1_d, dscr1_d);
	cuda::GpuMat tmp;
	cuda::cvtColor(img1_d, tmp, CV_BGR2GRAY);
	surf(tmp, cuda::GpuMat(), kp1_d, dscr1_d);
	tmp.release();


#ifdef _DEBUG
	Mat tt1;
	namedWindow("DBG");
#endif // _DEBUG

	for(int i = 1; i < imgs_d.size(); i++)
	{		
		cuda::GpuMat& img2_d = imgs_d[i];
		cuda::GpuMat kp2_d, dscr2_d;

		//find keypoints and descriptors
		cuda::GpuMat tmp;
		cuda::cvtColor(img2_d, tmp, CV_BGR2GRAY);
		surf(tmp, cuda::GpuMat(), kp2_d, dscr2_d);
		tmp.release();

		//match them
		matcher->clear();
		vector<DMatch> outliers, inliers;
		matcher->match(dscr2_d, dscr1_d, outliers);

		//filter some outliers
		double min_dist = 1e9;
		for each (const auto dm in outliers)
		{
			if (min_dist > dm.distance)
				min_dist = dm.distance;
		}

		for each (const auto dm in outliers)
		{
			if (dm.distance < RATIO * min_dist)
				inliers.push_back(dm);
		}

		//retrieve result from GPU
		Mat kp1_h, kp2_h, mask;
		kp1_d.download(kp1_h);
		kp2_d.download(kp2_h);
		vector<Point2f>	kp1(inliers.size());
		vector<Point2f>	kp2(inliers.size());
		

		//take only inliers kp
		int k = 0;
		for each (const auto dm in inliers)
		{			
			kp1[k] = Point2f(kp1_h.at<float>(0, dm.trainIdx), kp1_h.at<float>(1, dm.trainIdx));
			kp2[k] = Point2f(kp2_h.at<float>(0, dm.queryIdx), kp2_h.at<float>(1, dm.queryIdx));
		
			k++;
		}

		//filter inliers once more
		findHomography(kp2, kp1, mask, CV_RANSAC, 3);
		
		findTranslation(kp1, kp2, mask, &offset_accumulator);
		
#ifdef _DEBUG
		//displays the current image pair, waits for a key stroke on image window to proceed
		Mat tt2;
		if (i == 1)
			img1_d.download(tt1);
		img2_d.download(tt2);
		Mat gg = tt2.clone();
		for each (auto p in kp1)
		{
			cv::circle(tt1, p, 3, Scalar(0, 255, 0), 3);
		}
		for each (auto p in kp2)
		{
			cv::circle(tt2, p, 3, Scalar(0, 255, 0), 3);
		}

		Mat toshow(Size(tt1.cols * 2, tt1.rows), tt1.type());
		tt1.copyTo(toshow(Rect(0, 0, tt1.cols, tt1.rows)));
		tt2.copyTo(toshow(Rect(tt1.cols, 0, tt2.cols, tt2.rows)));
		auto trl = dbg_trasl[i - 1];
		line(toshow, Point(trl.x, 0), Point(trl.x, tt1.rows), Scalar(255, 0, 0), 2);
		imshow("DBG", toshow);
		waitKey(0);
		tt1 = gg;
#endif // _DEBUG

		//-------manage some GPU memory--------
		kp1_d.release();
		kp1_d = kp2_d.clone();
		kp2_d.release();
		dscr1_d.release();
		dscr1_d = dscr2_d.clone();
		dscr2_d.release();
	}

	//---------manage memory---------
	kp1_d.release();
	dscr1_d.release();
	surf.releaseMemory();
	matcher->clear();
}

void PanoramicImage::findTranslation(vector<Point2f> kp1, vector<Point2f> kp2, Mat mask, Point2f* inout_offset)
{
	Point2f avg(0, 0);
	Mat data_x, data_y;
	vector<Point2f> tmp;
	int s = 0;
	for (int i = 0; i < kp1.size(); i++)
	{
		if (!mask.at<bool>(i, 0))
			continue;

		const auto p2(kp2[i]);
		const auto p1(kp1[i]);
		const auto t = p1 - p2;
		tmp.push_back(t); 
		avg += t;

		data_x.push_back(t.x);
		data_y.push_back(t.y);
		s++;
	}

	avg /= (float)s;

	Mat cov_x(Size(s,s), CV_32FC1), cov_y(Size(s, s), CV_32FC1);

	for (int i = 0; i < s; i++)
	{
		const auto t1 = tmp[i];
		for (int j = 0; j < s; j++)
		{
			const auto t2 = tmp[j];
			cov_x.at<float>(i, j) = (t1 - avg).x * (t2 - avg).x;
			cov_y.at<float>(i, j) = (t1 - avg).y * (t2 - avg).y;
		}
	}

	Mat icov_x = cov_x.inv(DECOMP_SVD), icov_y = cov_y.inv(DECOMP_SVD);
	Mat col_ones = Mat::ones(Size(1, s), CV_32FC1); //col vector

	//weighted least squares
	Mat t_x = (col_ones.t() * icov_x * col_ones).inv() * col_ones.t() * icov_x * data_x; //should be a single float as result
	Mat t_y = (col_ones.t() * icov_y * col_ones).inv() * col_ones.t() * icov_y * data_y; //should be a single float as result
	 
#ifdef _DEBUG
	dbg_trasl.push_back(final_t);
#endif // _DEBUG

	(*inout_offset).x += t_x.at<float>(0, 0);

	trasl.push_back(Point2f((*inout_offset).x, t_y.at<float>(0, 0)));
}

void PanoramicImage::load_images(const string pattern, const int start, const double angle)
{
	int count = start;
	Mat img_h = Mat::zeros(1, 1, CV_8UC3);
	for ( ; ; count++)
	{
		char buff[256];

#ifdef OS_WIN
		sprintf_s(buff, 256, pattern.c_str(), count);
#else
		sprintf(buff, pattern.c_str(), count);
#endif
		
		string name(buff);
		img_h = imread(name, IMREAD_ANYCOLOR);
		if (img_h.empty())
			break;

		if (!img_h.empty())
		{
			img_h = PanoramicUtils::cylindricalProj(img_h, angle);
			cuda::GpuMat img_d(img_h.size(), img_h.type());
			img_d.upload(img_h);
			imgs_d.push_back(img_d);
		}
	}
}

PanoramicImage::~PanoramicImage()
{
#ifdef _DEBUG
	destroyAllWindows();
#endif // _DEBUG

	for each (auto img_d in imgs_d)
	{
		img_d.release();
	}
}
