#include "stdafx.h"
#include "Worker.h"

Worker::Worker(Mat* src, mutex* m, condition_variable* c):
	mtx (m),
	cond (c),
	done(false),
	t (&Worker::do_work, this), //start thread
	img_h(src)
{}

void Worker::do_work()
{
	Mat orig = img_h->clone();

	while (true)
	{
		unique_lock<mutex> lck(*mtx);
		cond->wait(lck);
		
		if (done)
			break;

#ifdef _DEBUG
		cout << "worker: loop" << endl;
#endif // _DEBUG

		if (event == EVENT_LBUTTONDOWN)
		{
#ifdef _DEBUG
			cout << "worker: threshold image" << endl;
#endif // _DEBUG

			cuda::GpuMat tmp(img_h->clone()), img_d;
			cuda::cvtColor(tmp, img_d, CV_BGR2HSV);

			Scalar mean = 0, std = 0;
			vector<cuda::GpuMat> channels;
			cuda::split(img_d, channels);

			cuda::GpuMat patch(channels[0], Rect(x - 1, y - 1, 3, 3)); //use only H channel
			cuda::meanStdDev(channels[0], mean, std);

			cuda::GpuMat mask, nmask, dst;
			cuda::absdiff(channels[0], mean[0], dst);
			cuda::threshold(dst, mask, 35, 255, THRESH_BINARY);
			
			int color = 45; //green
			//cuda::bitwise_and(channels[0], 0, channels[0], mask); //mask is bugged -> use copyTo
			cuda::bitwise_not(mask, nmask);
			channels[0].copyTo(dst, nmask);
			cuda::add(dst, color, dst, mask);

			channels[0] = dst;
			cuda::merge(channels, img_d);

			cuda::cvtColor(img_d, tmp, CV_HSV2BGR);
			tmp.download(*img_h);

			channels[0].release();
			channels[1].release();
			channels[2].release();
			dst.release();
			mask.release();
			nmask.release();
			tmp.release();
			patch.release();
			img_d.release();
		}
		else
		{
			//reset image
#ifdef _DEBUG
			cout << "worker: reset image" << endl;
#endif // _DEBUG

			*img_h = orig.clone();
		}
	}
}

void Worker::setMouseInfo(int _event, int _x, int _y)
{
	event = _event; 
	x = _x;
	y = _y;
}

void Worker::stop_work()
{
	done = true;
	unique_lock<mutex> lck(*mtx);
	cond->notify_one();
}

Worker::~Worker()
{
	t.join();
}
