#pragma once

#include "PanoramicUtils.h"

#include <opencv2/core/cuda.hpp>
#include <opencv2/cudafeatures2d.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/stitching.hpp>


#define RATIO 2.5
#if defined(_WIN32) || defined(WIN32) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__BORLANDC__) || defined (_WINDOWS_)
#define OS_WIN
#endif

class PanoramicImage
{
private:
	std::vector<cv::cuda::GpuMat> imgs_d;
	std::vector<cv::Point2f> trasl;
#ifdef _DEBUG
	std::vector<cv::Point2f> dbg_trasl;
#endif

	void findSURF();
	void findTranslation(std::vector<cv::Point2f> kp1, std::vector<cv::Point2f> kp2, cv::Mat mask, cv::Point2f* offset);

	void load_images(const std::string pattern, const int start, const double angle);


public:
	PanoramicImage(const std::string pattern, const int start, const double angle);

	cv::Mat createPanorama();
	
	/* Just for ground truth*/
	//cv::Mat createPanorama2();

	~PanoramicImage();

};

