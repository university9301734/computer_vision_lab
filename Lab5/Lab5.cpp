// Lab5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <stdio.h>
#include <iostream>
#include <opencv2/highgui.hpp>


#include "PanoramicImage.h"

int main()
{
	using namespace cv;
	using namespace std;
	PanoramicImage panorama("..//DATA//Lab5//i%02d.bmp", 1, 33);
	Mat pano = panorama.createPanorama();
	imwrite("..//DATA//Lab5//output.png", pano);
	namedWindow("OUTPUT");
	imshow("OUTPUT", pano);
	waitKey(0);

	destroyAllWindows();

	return 0;
}

