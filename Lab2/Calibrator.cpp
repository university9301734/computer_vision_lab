#include "stdafx.h"
#include "Calibrator.h"

Calibrator::Calibrator(int width, int height, float length, const vector<string>& args, int flags, int max_live_snapshots) :
	x(width),
	y(height),
	m(max_live_snapshots),
	l(length),
	f(flags),
	a(args)
{
	if (f & USE_IMAGE_SEQUENCE)
	{
		try
		{
			if (!dirExists(args[0]))
				throw Exception();

			//cap = new VideoCapture(args[0] + "\\" + args[1]);	//BUG: see code of from_storage()
		}
		catch (Exception e)
		{
			cerr << "[Calibrator] Expecting a path to a valid *directory* in args[0] and a *file name pattern* in args[1] (e.g. img%02d.jpg to read img00.jpg, img01.jpg, img02.jpg ...)" << endl;
			cerr << e.what() << endl;
			exit(1);
		}
	}
	else if (f & USE_VIDEO_FILE)
	{
		try
		{
			cap = new VideoCapture(args[0]);
		}
		catch (Exception e)
		{
			cerr << "[Calibrator] Expecting a path to a valid video file in args[0] " << endl;
			cerr << e.what() << endl;
			exit(1);
		}
	}
	else if (f & USE_LIVE_CAM_FEED)
	{
		try
		{
			int cam_idx = stoi(args[0]);
			cap = new VideoCapture(cam_idx);
		}
		catch (Exception e)
		{
			cerr << "[Calibrator] Expecting a valid *int* in args[0] (encoded as string, e.g. args[0] = std::to_string(<some int>)), indicating an available camera connected to the system. See 'OpenCV VideoCapture' for more details." << endl;
			cerr << e.what() << endl;
			exit(1);
		}
	}
	else
	{
		cerr << "[Calibrator] incorrect flags" << endl;
		exit(1);
	}

	if (!(f & USE_IMAGE_SEQUENCE))
		assert(cap->isOpened());

	if (f & USE_LIVE_CAM_FEED)
	{
		cv::namedWindow(WIN_NAME, CV_WINDOW_NORMAL);
	}

#if defined(_DEBUG)
	if (!(f & USE_LIVE_CAM_FEED))
	{
		cv::namedWindow(WIN_NAME, CV_WINDOW_NORMAL);
	}
#endif //_DEBUG
 
}

vector<Point3f> Calibrator::createChessGrid()
{
	vector< Point3f> tmp;
	for (int i = 0; i < y; ++i)
		for (int j = 0; j < x; ++j)
			tmp.push_back(Point3f(j*l, i*l, 0));

	return tmp;
}

vector<Point3f> Calibrator::createCircleGrid()
{
	vector< Point3f> tmp;
	for (int i = 0; i < y; i++)
		for (int j = 0; j < x; j++)
			tmp.push_back(Point3f((2 * j + i % 2)*l, i*l, 0));

	return tmp;
}

int Calibrator::from_storage(vector< vector<Point2f> >& corners, Size& img_size)
{
	int count = 1;
	Mat color_img, img;
	do
	{
		if (f & USE_VIDEO_FILE)
		{
			(*cap) >> color_img;			
		}
		else //VideoCapture doesn't handle EXIF info on still images (imread() does instead), but calibration may fail if image orientation isn't accounted for.
		{
			char buff[256];
			sprintf_s(buff, 256, a[1].c_str(), count);
			color_img = imread(a[0] + "\\" + string(buff), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
		}


		if (color_img.empty())
			break;

		if (count == 1)
			img_size = color_img.size();
		
		if (color_img.type() == CV_8UC1)
			img = color_img.clone();
		else
			cvtColor(color_img, img, CV_BGR2GRAY);	

		vector<Point2f> c;
		bool ok;
		if (f & USE_CIRCLE_GRID)
			ok = findCirclesGrid(img, Size(x, y), c, CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_FAST_CHECK | CALIB_CB_NORMALIZE_IMAGE);
		else
			ok = findChessboardCorners(img, Size(x, y), c, CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_FAST_CHECK | CALIB_CB_NORMALIZE_IMAGE);

		if (ok && c.size() == x * y)
		{
			cornerSubPix(img, c, Size(11, 11), Size(-1, -1), TermCriteria(TermCriteria::COUNT | TermCriteria::EPS, 100, 1e-9));
			corners.push_back(c);

			count++;
		}

#ifdef _DEBUG  
		if (!(f & OVERRIDE_MANUAL_DEBUG))
		{
			Mat& to_show = color_img;
			drawChessboardCorners(to_show, Size(x, y), corners.back(), ok);
			putText(to_show, "PRESS n FOR NEXT IMAGE", Point(20, color_img.size().height - 20), FONT_HERSHEY_PLAIN, 3., Scalar(0, 255, 0));
			do { imshow(WIN_NAME, to_show); } while (waitKey(50) != 'n'); //wait user input
		}
#endif //_DEBUG

	} while (!color_img.empty());

	return count - 1;
}


int Calibrator::from_live_feed(vector< vector<Point2f> >& corners, Size& img_size)
{
	int count = 0;
	char b = 0;
	Mat color_img, img;
	while (count < m && b != 27)
	{
		(*cap) >> color_img;		
		if (color_img.empty())
			break;

		if (count == 0)
			img_size = color_img.size();

		cvtColor(color_img, img, CV_BGR2GRAY);

		vector<Point2f> c;
		bool ok;
		if (f & USE_CIRCLE_GRID)
			ok = findCirclesGrid(img, Size(x, y), c);
		else
			ok = findChessboardCorners(img, Size(x, y), c);

		if (ok && c.size() == x * y)
		{
			cornerSubPix(img, c, Size(11, 11), Size(-1, -1), TermCriteria(TermCriteria::COUNT | TermCriteria::EPS, 100, 1e-9));
			corners.push_back(c);

			count++;
		}

		if (f & USE_LIVE_CAM_FEED)
		{
			if (!corners.empty())
			{
				drawChessboardCorners(color_img, Size(x, y), corners.back(), ok);
			}
			putText(color_img, "PRESS esc TO STOP GRID ACQUISITION AND START CALIBRATION (stops automatically in " + to_string(count) + "/" + to_string(m) + ")",
				Point(color_img.size().height - 20, 20), FONT_HERSHEY_PLAIN, 2., Scalar(0, 255, 0));			
			imshow(WIN_NAME, color_img);
			b = waitKey(ok ? 2000 : 50);
		}
	}

	return count;
}

double Calibrator::calibrate(struct calibration* out_res)
{
	int n = 0;
	vector< vector<Point2f> > corners;
	Size s;
	cout << "[CALIBRATOR]: loading/acquiring images" << endl;
	if (f & USE_LIVE_CAM_FEED)
		n = from_live_feed(corners, s);
	else
		n = from_storage(corners, s);

	vector< vector< Point3f> > virtual_points(n);
	vector< Point3f> tmp;
	if (f & USE_CIRCLE_GRID)
		tmp = createCircleGrid();
	else
		tmp = createChessGrid();

	for (int i = 0; i < n; i++) virtual_points[i] = tmp;
	tmp.clear();

	cout << "[CALIBRATOR]: starting calibration" << endl;
	if (n < 5)
	{
		cout << "[CALIBRATOR]: Too few images (less than 5) supplied. Aborting." << endl;
		return -1; //To few images
	}

	Mat cam_mat, dist_coeffs;
	double avgRMSerr = -1;
	vector<Mat> rvec, tvec;
	vector<double> RMSs, stdErrInt, stdErrExt;
	
	struct pair
	{
		double err;
		int idx;
	};

	avgRMSerr = calibrateCamera(
		virtual_points,
		corners,
		s,
		cam_mat,
		dist_coeffs,
		rvec,
		tvec,
		stdErrInt,
		stdErrExt,
		RMSs,
		0, //flags
		TermCriteria(TermCriteria::COUNT | TermCriteria::EPS, 100, DBL_EPSILON)
	);
	
	//Filter out outliers based on Quartiles
	vector<struct pair> stats(n);
	for (int i = 0; i < stats.size(); i++)
	{
		struct pair p;
		p.err = RMSs[i];
		p.idx = i;
		stats[i] = p;
	}

	//Naive sort
	for (int i = 0; i < stats.size(); i++)
	{
		for (int j = 0; j < stats.size(); j++)
		{
			if (stats[i].err < stats[j].err)
			{
				struct pair p = stats[j];
				stats[j] = stats[i];
				stats[i] = p;
			}
		}
	}

	// first and third Quartiles idx
	double up_range = 0;
	if ((n / 2) % 2 == 0) //use median as splitter, then find median on the two halves
	{
		int q1_1 = cvFloor(n * 0.25) - 1; //we are dealing with vector's indexies that starts from 0
		int q1_2 = cvCeil(n * 0.25) - 1; //we are dealing with vector's indexies that starts from 0
		int q3_1 = cvFloor(n * 0.75);
		int q3_2 = cvCeil(n * 0.75);
		double k = 1.5; //control the range
		double diff = (stats[q3_1].err + stats[q3_2].err) / 2 - (stats[q1_1].err + stats[q1_2].err) / 2;
		up_range = diff * k + (stats[q3_1].err + stats[q3_2].err) / 2;
	}
	else
	{
		int q1 = cvFloor(n * 0.25);
		int q3 = cvFloor(n * 0.75);
		double k = 1.5; //control the range
		double diff = stats[q3].err - stats[q1].err;
		up_range = diff * k + stats[q3].err;
	}
	
	vector< vector<Point2f> > corners_new;
	vector< vector<Point3f> > virtual_points_new;
	for (int i = 0; i < stats.size(); i++)
	{
		if (stats[i].err <= up_range)
		{
			virtual_points_new.push_back(virtual_points[stats[i].idx]);
			corners_new.push_back(corners[stats[i].idx]);
		}
	}

	RMSs.clear();
	stdErrInt.clear();
	stdErrExt.clear();
	rvec.clear();
	tvec.clear();
	avgRMSerr = calibrateCamera(
		virtual_points_new,
		corners_new,
		s,
		cam_mat,
		dist_coeffs,
		rvec,
		tvec,
		stdErrInt,
		stdErrExt,
		RMSs,
		0, //flags
		TermCriteria(TermCriteria::COUNT | TermCriteria::EPS, 100, DBL_EPSILON)
	);

	out_res->cam_mat = cam_mat.clone();
	out_res->dist_coeffs = dist_coeffs.clone();
	out_res->tot_grid_views = corners_new.size();
	if (f & USE_FULL_OUTPUT)
	{
		out_res->full_output = true;
		out_res->rvecs = rvec;
		out_res->tvecs = tvec;
		out_res->stdDevInt = stdErrInt;
		out_res->stdDevExt = stdErrExt;
		out_res->perViewError = RMSs;
	}
	else
	{
		out_res->full_output = false;
		out_res->rvecs = vector<Mat>(0);
		out_res->tvecs = vector<Mat>(0);
		out_res->stdDevInt = vector<double>(0);
		out_res->stdDevExt = vector<double>(0);
		out_res->perViewError = vector<double>(0);
	}

	return avgRMSerr;
}



void Calibrator::save_to_file(const string path, struct calibration* c)
{
	FileStorage fs(path, FileStorage::WRITE);
	if (!fs.isOpened())
	{
		cerr << "[CALIBRATOR] Can't open file " + path << ".\n\tMake sure the folder already exists."<< endl;
		return;
	}

	fs << "CM" << c->cam_mat;
	fs << "DC" << c->dist_coeffs;
	fs << "FO" << c->full_output;
	fs << "GV" << c->tot_grid_views;

	if (c->full_output)
	{		
		fs << "RV" << c->rvecs;
		fs << "TV" << c->tvecs;
		fs << "DI" << c->stdDevInt;
		fs << "DE" << c->stdDevExt;
		fs << "VE" << c->perViewError;
	}

	fs.release();
}

void Calibrator::load_from_file(const string path, struct calibration* out_res)
{
	FileStorage fs(path, FileStorage::READ);
	if (!fs.isOpened())
	{
		cerr << "[CALIBRATOR] Can't load from file " + path << endl;
		return;
	}

	fs["CM"] >> out_res->cam_mat;
	fs["DC"] >> out_res->dist_coeffs;
	fs["FO"] >> out_res->full_output;
	fs["GV"] >> out_res->tot_grid_views;

	if (out_res->full_output)
	{
		fs["RV"] >> out_res->rvecs;
		fs["TV"] >> out_res->tvecs;
		fs["DI"] >> out_res->stdDevInt;
		fs["DE"] >> out_res->stdDevExt;
		fs["VE"] >> out_res->perViewError;
	}

	fs.release();
}

Calibrator::~Calibrator()
{
	if (cap != nullptr)
	{
		cap->release();
		delete cap;
	}
}
